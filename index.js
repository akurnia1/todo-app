// clickMe = () => {
//   //   let todo = document.getElementById("todo");
//   //   todo.innerHTML = "<li> mengerjakan pekerjaan rumah </li>";
//   let paragraphs = document.getElementsByClassName("paragraph-banner");
//   //   console.log("paragraphs", paragraphs.length);
//   for (let i = 0; i < paragraphs.length; i++) {
//     console.log("paragraph[i]", i, paragraphs[i]);
//     paragraphs[i].style.color = "red";
//   }
// };

// contentAdd = () => {
//   let newContent = document.createElement("h1");
//   newContent.textContent = "ini tambahan";
//   let idContent = document.getElementById("element-baru");
//   idContent.append(newContent);
// };

// setInterval(() => {
//   console.log("interval");
// }, 1000);

let todos = [];

function deleteTodo(id) {
  //   console.log("id", id);
  let filteredTodo = todos.filter((todo) => {
    return todo.id != id;
  });
  console.log("filteredTodo", filteredTodo);
  todos = filteredTodo;
  localStorage.setItem("todos", JSON.stringify(todos));
  getTodoList();
}
function updateTodo(id) {
  //   console.log("od update", id);
  todos.forEach((todo) => {
    if (todo.id == id) {
      // cara keren
      todo.status = !todo.status;
      // cara usang
      //   if (todo.status == false) {
      //     todo.status = true;
      //   } else {
      //     todo.status = false;
      //   }
    }
  });
  localStorage.setItem("todos", JSON.stringify(todos));
  getTodoList();
}

function getStatusTodo(value) {
  if (value == true) {
    return "Done";
  }
  return "On Going";
}
function getTodoList() {
  let storageItems = localStorage.getItem("todos");
  // data yang diambil dari local storage harus dibalikin menjadi json object lagi
  todos = JSON.parse(storageItems);
  let ulTodo = document.getElementById("todo-list");
  ulTodo.innerHTML = "";
  todos.forEach((todo) => {
    let newTodo = document.createElement("li");
    // cara 1
    newTodo.textContent = todo.name;
    // cara 2
    // newContent.textContent = todo["name"]
    newTodo.innerHTML = `
    <li> ${todo.name} - status : ${getStatusTodo(todo.status)}</li>
    <button onclick="deleteTodo(${todo.id})"> delete </button>
    <button onclick="updateTodo(${todo.id})"> update status </button>
    `;

    ulTodo.append(newTodo);
  });
}

getTodoList();

function createTodo() {
  let maxID = Math.floor(Math.random() * (1000000 + 1));
  let valueTask = document.getElementById("input-todo").value;
  if (valueTask.length > 3) {
    let objectTask = {
      id: maxID,
      name: valueTask,
      status: false,
    };
    todos.push(objectTask);
    localStorage.setItem("todos", JSON.stringify(todos));
    getTodoList();
    document.getElementById("input-todo").value = "";
    // object json dengan stringify dibuat menjadi string suapya bisa disimpen kedalam localstorage
  }
  //   alert("create todo");
}

let inputListener = document.getElementById("input-todo");
inputListener.addEventListener("keyup", function (event) {
  if (event.key === "Enter") {
    createTodo();
    // Do work
  }
});

// crud
// create - done
// read - done
// update
// delete - Done

// tutorial OOP
// class Todo {
//   constructor() {
//     this.todos;
//   }

//   createTodo() {}
// }
